﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : System.IDisposable
{
	public const int maxPoints = 100;

	private readonly MeshRenderer chunkRenderer;
	private readonly Vector2 parentOffset;
	public readonly Rect bounds;

	private Vector4[] points = new Vector4[maxPoints];
	private Vector4[] pointColours = new Vector4[maxPoints];

	private int total;
	
	public Chunk (MeshRenderer chunkRenderer, Vector2 parentOffset, Rect bounds)
	{
		this.chunkRenderer = chunkRenderer;
		this.parentOffset = parentOffset;
		this.bounds = bounds;

		chunkRenderer.gameObject.name += bounds.ToString ();
	}

	public void AddToken (Token token, Color colour)
	{
		if (total < points.Length)
		{
			points[total] = new Vector4 (token.position.x - parentOffset.x, token.position.y - parentOffset.y, token.currentSize, token.level);
			pointColours[total] = colour;

			total++;
		}
		else
		{
			Debug.LogError ("Chunk.AddToken: Reached maximum points");
		}
	}

	public void ClearTokens ()
	{
		total = 0;
	}

	public void Complete ()
	{
		if (total > 0)
		{
			chunkRenderer.gameObject.SetActive (true);
			chunkRenderer.transform.localPosition = parentOffset;
			chunkRenderer.material.SetInt ("numPoints", total);
			chunkRenderer.material.SetVectorArray ("points", points);
			chunkRenderer.material.SetVectorArray ("pointColours", pointColours);
		}
		else
		{
			chunkRenderer.gameObject.SetActive (false);
		}
	}

	public void Dispose ()
	{
		if (chunkRenderer != null)
		{
			Object.Destroy (chunkRenderer.material);
			Object.Destroy (chunkRenderer.gameObject);
		}
	}
}
