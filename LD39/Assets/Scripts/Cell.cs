﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
	private struct CollisionEvent
	{
		public readonly Token a;
		public readonly Token b;
		public readonly float sqrDistance;

		public CollisionEvent (Token a, Token b, float sqrDistance)
		{
			this.a = a;
			this.b = b;
			this.sqrDistance = sqrDistance;
		}

		public void Run ()
		{
			if (a.isAlive && b.isAlive) a.OnTokenCollision (b, sqrDistance);
		}
	}

	public const float collisionDistance = 0.01f;
	public const float overShootFactor = 0.5f;

	public List<Token> tokens = new List<Token> ();
	public List<Token> newTokens = new List<Token> ();
	private Token[] tokensArray;
	private Vector2[] forces;

	public readonly Map map;
	public readonly int x;
	public readonly int y;
	public readonly float leftEdge;
	public readonly float rightEdge;
	public readonly float topEdge;
	public readonly float bottomEdge;

	public Cell Up { get; private set; }
	public Cell UpLeft { get; private set; }
	public Cell UpRight { get; private set; }
	public Cell Left { get; private set; }
	public Cell Right { get; private set; }
	public Cell Down { get; private set; }
	public Cell DownLeft { get; private set; }
	public Cell DownRight { get; private set; }

	public Token[] Tokens
	{
		get
		{
			return tokensArray;
		}
	}

	public Cell (Map map, int x, int y)
	{
		this.map = map;
		this.x = x;
		this.y = y;

		leftEdge = x;
		rightEdge = x + 1;
		bottomEdge = y;
		topEdge = y + 1;
	}

	public void CalculateNeighbours ()
	{
		Up			= map[x + 0, y + 1];
		UpLeft		= map[x - 1, y + 1];
		UpRight		= map[x + 1, y + 1];
		Left		= map[x - 1, y + 0];
		Right		= map[x + 1, y + 0];
		Down		= map[x + 0, y - 1];
		DownLeft	= map[x - 1, y - 1];
		DownRight	= map[x + 1, y - 1];
	}

	public void AddToken (Token token)
	{
		if (!tokens.Contains (token)) tokens.Add (token);
	}

	public void RemoveToken (Token token)
	{
		if (tokens.Contains (token)) tokens.Remove (token);
	}

	public void CheckTokenCellPositions ()
	{
		Vector2 pos;

		for (int i = 0; i < tokens.Count; i++)
		{
			if (tokens[i] != null)
			{
				pos = tokens[i].position;

				if (pos.x < leftEdge && Left != null)
				{
					if (pos.y < bottomEdge && DownLeft != null) DownLeft.newTokens.Add (tokens[i]);
					else if (pos.y >= topEdge && UpLeft != null) UpLeft.newTokens.Add (tokens[i]);
					else Left.newTokens.Add (tokens[i]);
				}
				else if (pos.x >= rightEdge && Right != null)
				{
					if (pos.y < bottomEdge && DownRight != null) DownRight.newTokens.Add (tokens[i]);
					else if (pos.y >= topEdge && UpRight != null) UpRight.newTokens.Add (tokens[i]);
					else Right.newTokens.Add (tokens[i]);
				}
				else
				{
					if (pos.y < bottomEdge && Down != null) Down.newTokens.Add (tokens[i]);
					else if (pos.y >= topEdge && Up != null) Up.newTokens.Add (tokens[i]);
					else this.newTokens.Add (tokens[i]);
				}
			}
			else
			{
				Debug.Log ("Saved having a null reference exception");
			}
		}
	}

	public void PushNewTokens ()
	{
		tokens = new List<Token> (newTokens);
		tokensArray = tokens.ToArray ();
		newTokens.Clear ();
		forces = new Vector2[tokens.Count];
	}

	public void CheckCollisions ()
	{
		Queue<CollisionEvent> collisionQueue = new Queue<CollisionEvent> ();

		Vector2 thisPos, otherPos;
		float sqrDistance, overlap, deltaX, deltaY, minDist;

		for (int a = tokensArray.Length - 1; a >= 0; a--)
		{
			thisPos = tokensArray[a].position;

			for (int b = tokensArray.Length - 1; b >= 0; b--)
			{
				if (a != b)
				{
					otherPos = tokensArray[b].position;
					deltaX = thisPos.x - otherPos.x;
					deltaY = thisPos.y - otherPos.y;
					sqrDistance = (deltaX * deltaX) + (deltaY * deltaY);
					minDist = (tokensArray[a].currentSize + tokensArray[b].currentSize) * (tokensArray[a].currentSize + tokensArray[b].currentSize);
					if (tokensArray[a].team != tokensArray[b].team) minDist *= 9;
					overlap = minDist - sqrDistance;
					if (overlap > 0)
					{
						forces[a].x += deltaX * overShootFactor;
						forces[a].y += deltaY * overShootFactor;
						//tokensArray[a].OnTokenCollision (tokensArray[b], sqrDistance);
						collisionQueue.Enqueue (new CollisionEvent (tokensArray[a], tokensArray[b], sqrDistance));
					}
				}
			}

			updateForces (Up, a, thisPos, ref collisionQueue);
			updateForces (UpLeft, a, thisPos, ref collisionQueue);
			updateForces (UpRight, a, thisPos, ref collisionQueue);
			updateForces (Left, a, thisPos, ref collisionQueue);
			updateForces (Right, a, thisPos, ref collisionQueue);
			updateForces (Down, a, thisPos, ref collisionQueue);
			updateForces (DownLeft, a, thisPos, ref collisionQueue);
			updateForces (DownRight, a, thisPos, ref collisionQueue);

			// Update for map edges
			if (thisPos.x < 2) forces[a].x++;
			else if (thisPos.x > map.size - 2) forces[a].x--;

			if (thisPos.y < 2) forces[a].y++;
			else if (thisPos.y > map.size - 2) forces[a].y--;

			// Update for obstacles
			Obstacle currentObstacle;
			float size = tokensArray[a].currentSize;

			for (int i = 0; i < map.obstacles.Count; i++)
			{
				currentObstacle = map.obstacles[i];

				for (int b = 0; b < currentObstacle.rotatedPoints.Count; b++)
				{
					deltaX = thisPos.x - currentObstacle.rotatedPoints[b].x;
					deltaY = thisPos.y - currentObstacle.rotatedPoints[b].y;
					sqrDistance = (deltaX * deltaX) + (deltaY * deltaY);
					minDist = (tokensArray[a].currentSize + 0.25f) * (tokensArray[a].currentSize + 0.25f);
					overlap = minDist - sqrDistance;
					if (overlap > 0)
					{
						forces[a].x += deltaX;
						forces[a].y += deltaY;
					}
				}
			}
		}

		for (int i = 0; i < tokensArray.Length; i++)
		{
			tokensArray[i].velocity += forces[i];
			tokensArray[i].position += (tokensArray[i].velocity * Time.deltaTime);
			tokensArray[i].velocity -= forces[i] * 0.75f;
		}

		// Run all the queued collision events
		while (collisionQueue.Count > 0) collisionQueue.Dequeue ().Run ();
	}

	public void GetTokensInRange (Vector2 position, float range, List<Token> currentTokens)
	{
		for (int i = 0; i < tokens.Count; i++)
		{
			if (Vector2.Distance (tokens[i].position, position) < range + tokens[i].currentSize && !currentTokens.Contains (tokens[i])) currentTokens.Add (tokens[i]);
		}
	}

	public void GetTokensInArea (Vector2 centrePosition, Vector2 size, List<Token> currentTokens)
	{
		throw new System.NotImplementedException ();
	}

	void updateForces (Cell otherCell, int a, Vector2 thisPos, ref Queue<CollisionEvent> collisionQueue)
	{
		if (otherCell != null && otherCell.tokensArray != null)
		{
			Vector2 otherPos;
			float sqrDistance, overlap, deltaX, deltaY, minDist;

			for (int b = otherCell.tokensArray.Length - 1; b >= 0; b--)
			{
				otherPos = otherCell.tokensArray[b].position;
				deltaX = thisPos.x - otherPos.x;
				deltaY = thisPos.y - otherPos.y;
				sqrDistance = (deltaX * deltaX) + (deltaY * deltaY);
				minDist = (tokensArray[a].currentSize + otherCell.tokensArray[b].currentSize) * (tokensArray[a].currentSize + otherCell.tokensArray[b].currentSize);
				if (tokensArray[a].team != otherCell.tokensArray[b].team) minDist *= 4;
				overlap = minDist - sqrDistance;
				if (overlap > 0)
				{
					forces[a].x += deltaX * overShootFactor;
					forces[a].y += deltaY * overShootFactor;
					//tokensArray[a].OnTokenCollision (otherCell.tokensArray[b], sqrDistance);
					collisionQueue.Enqueue (new CollisionEvent (tokensArray[a], otherCell.tokensArray[b], sqrDistance));
				}
				else if (overlap > -collisionDistance)
				{
					//tokensArray[a].OnTokenCollision (otherCell.tokensArray[b], sqrDistance);
					collisionQueue.Enqueue (new CollisionEvent (tokensArray[a], otherCell.tokensArray[b], sqrDistance));
				}
			}
		}
	}
}
