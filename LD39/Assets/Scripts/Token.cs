﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token
{
	private struct OverlapTimerData
	{
		public float time;
		public bool isStillOverlapping;
	}

	public const float sqrMergePercent = 0.25f;

	public readonly Team team;

	//private Rigidbody2D rb;
	//private CircleCollider2D cc;

	public float friction = 1f;
	public uint level = 1;
	public float currentSize = 0;
	public float maxHealthPerLevel = 5;
	public float currentHealth;
	public bool isAlive = true;

	public float maxHealth
	{
		get
		{
			return maxHealthPerLevel * level;
		}
	}

	public Vector2 position;
	public Vector2 velocity;

	Dictionary<Token, OverlapTimerData> overlapTimers = new Dictionary<Token, OverlapTimerData> ();

	public void OnTokenCollision (Token other, float sqrDistance)
	{
		if (this.team != other.team)
		{
			other.ApplyDamage (Time.deltaTime * level * 2);
		}
		else
		{
			if (sqrDistance < sqrMergePercent * currentSize * currentSize)
			{
				if (!overlapTimers.ContainsKey (other)) overlapTimers[other] = new OverlapTimerData () { time = Time.deltaTime, isStillOverlapping = true, };
				else if (overlapTimers[other].time < 2)
				{
					OverlapTimerData data = overlapTimers[other];
					data.time = overlapTimers[other].time + Time.deltaTime;
					data.isStillOverlapping = true;
					overlapTimers[other] = data;
				}
				else
				{
					overlapTimers.Remove (other);

					if (level < other.level)
					{
						if (other.level < other.team.maxLevel)
						{
							other.level++;
							other.currentHealth += currentHealth;
						}
						Destroy ();
					}
					else
					{
						if (level < team.maxLevel)
						{
							level++;
							currentHealth += other.currentHealth;
						}
						other.Destroy ();
					}
				}
			}
		}
	}

	public Token (Team team, Vector2 position)
	{
		this.team = team;
		this.position = position;

		currentHealth = maxHealthPerLevel;
		currentSize = 0;
	}

	public void Update (float deltaTime)
	{
		// Remove any invalid overlap timers
		Queue<Token> toRemove = new Queue<Token> ();
		foreach (KeyValuePair<Token, OverlapTimerData> item in overlapTimers) if (!item.Value.isStillOverlapping) toRemove.Enqueue (item.Key);
		while (toRemove.Count > 0) overlapTimers.Remove (toRemove.Dequeue ());

		OverlapTimerData data;
		List<Token> keys = new List<Token> (overlapTimers.Keys);
		for (int i = 0; i < keys.Count; i++)
		{
			data = overlapTimers[keys[i]];
			data.isStillOverlapping = false;
			overlapTimers[keys[i]] = data;
		}

		if (position.x < 1 || position.x > team.map.size - 1 || position.y < 1 || position.y > team.map.size - 1) ApplyDamage (Time.deltaTime * 5 * level);

		Building closest = null;
		float minDist = 0;
		float tmpDist;

		if (team.buildings.Count > 0)
		{
			for (int i = 0; i < team.buildings.Count; i++)
			{
				tmpDist = Vector2.SqrMagnitude ((Vector2)team.buildings[i].transform.position - position);
				if (closest == null || tmpDist < minDist)
				{
					closest = team.buildings[i];
					minDist = tmpDist;
				}
			}
		}
		else
		{
			for (int i = 0; i < team.map.buildings.Count; i++)
			{
				tmpDist = Vector2.SqrMagnitude ((Vector2)team.map.buildings[i].transform.position - position);
				if (closest == null || tmpDist < minDist)
				{
					closest = team.map.buildings[i];
					minDist = tmpDist;
				}
			}
		}

		velocity += ((Vector2)closest.transform.position - position).normalized * Time.deltaTime * 1.2f;

		if (currentHealth > 0) currentSize = Mathf.Lerp (currentSize, team.tokenSize * 1.75f * Mathf.Max (currentHealth, maxHealth * 0.5f) / Mathf.Max (1, level - 1) / maxHealthPerLevel, Time.deltaTime);
		else
		{
			Destroy ();
			Sound.PlayPopSound ();
		}

		velocity = Vector2.ClampMagnitude (velocity, Mathf.Max (velocity.magnitude - (friction * Time.deltaTime), 0));
	}

	public void ApplyDamage (float amount)
	{
		if (currentHealth > 0)
		{
			currentHealth -= amount;
			if (level > 1 && currentHealth < maxHealth - maxHealthPerLevel) level--;
		}
	}

	public void Destroy ()
	{
		isAlive = false;
		team.RemoveToken (this);
	}
}
