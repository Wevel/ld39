﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITeam : Team
{
	public AITeam (Map map, Color colour, float baseTokenSize) : base (map, colour, baseTokenSize) { }

	float updateDelay = 4;
	float updateTimer = Random.Range (-2f, 2f);
	float weightCutoff = 10;

	public override void Update (float deltaTime)
	{
		if (updateTimer < updateDelay)
		{
			updateTimer += deltaTime * buildings.Count;
		}
		else
		{
			updateTimer -= updateDelay;

			List<AIAction> options = new List<AIAction> ();

			for (int i = 0; i < buildings.Count; i++)
			{
				for (int a = i + 1; a < buildings.Count; a++)
				{
					options.Add (new AIActionDefend (this, buildings[a], buildings[i]));
				}

				for (int a = 0; a < map.buildings.Count; a++)
				{
					if (map.buildings[a].currentTeam != this && Vector2.Distance (buildings[i].transform.position, map.buildings[a].transform.position) < 4 * buildings[i].level)
						options.Add (new AIActionAttack (this, map.buildings[a], buildings[i]));
				}
			}

			if (options.Count > 0)
			{
				options.Sort ((a, b) => { return -a.weight.CompareTo (b.weight); });
				if (options[0].weight > weightCutoff) options[0].DoAction ();
			}
			else
			{
				Debug.LogWarning ("AITeam.Update: Somehow there are no posible actions the team can make");
			}
		}
	}

	public override void OnLoseBuilding (Building building)
	{

	}

	public override void OnGainBuilding (Building building)
	{

	}
}
