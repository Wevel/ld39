﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionDefend : AIAction
{
	public AIActionDefend (AITeam team, Building target, Building source) : base (team, target, source, 2f) { }

	protected override float getWeight ()
	{
		float total = 0;

		List<Token> sourceTokens = team.map.GetTokensInRange (source.transform.position, sourceRange);

		for (int i = 0; i < sourceTokens.Count; i++)
		{
			if (sourceTokens[i].team == team) total += 0.5f * sourceTokens[i].level;
		}

		List<Token> targetTokens = team.map.GetTokensInRange (target.transform.position, sourceRange);

		for (int i = 0; i < targetTokens.Count; i++)
		{
			if (targetTokens[i].team == team) total -= 1 * targetTokens[i].level;
			else if (targetTokens[i].team != team) total += 1 * targetTokens[i].level;
		}

		total += 5 / Mathf.Clamp (target.teamControl, 0.25f, 1);

		total *= target.level;
		total /= Vector2.Distance (target.transform.position, source.transform.position);

		return total;
	}
}
