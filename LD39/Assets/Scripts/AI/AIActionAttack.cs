﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionAttack : AIAction
{
	public AIActionAttack (AITeam team, Building target, Building source) : base (team, target, source, 2f) { }

	protected override float getWeight ()
	{
		if (target == source) return float.MinValue;

		float total = 0;

		List<Token> sourceTokens = team.map.GetTokensInRange (source.transform.position, sourceRange);

		for (int i = 0; i < sourceTokens.Count; i++)
		{
			if (sourceTokens[i].team == team) total += 1 * sourceTokens[i].level;
		}

		List<Token> targetTokens = team.map.GetTokensInRange (target.transform.position, sourceRange);

		for (int i = 0; i < targetTokens.Count; i++)
		{
			if (targetTokens[i].team != team) total -= 0.35f * targetTokens[i].level;
		}

		if (target.currentTeam != null || target.newTeam != team) total += 5 / Mathf.Clamp (target.teamControl, 0.25f, 1);
		else total += 20 / target.level;

		total /= target.level;
		total /= Vector2.Distance (target.transform.position, source.transform.position);

		return total;
	}
}
