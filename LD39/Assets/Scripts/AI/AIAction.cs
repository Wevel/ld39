﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIAction
{
	public readonly AITeam team;
	public readonly Building target;
	public readonly Building source;
	public readonly float sourceRange;
	public readonly float weight;

	public AIAction (AITeam team, Building target, Building source, float sourceRange)
	{
		this.team = team;
		this.target = target;
		this.source = source;
		this.sourceRange = sourceRange;
		this.weight = getWeight ();
	}

	public void DoAction ()
	{
		List<Token> sourceTokens = team.map.GetTokensInRange (source.transform.position, sourceRange);
		List<Token> group = new List<Token> ();
		for (int i = 0; i < sourceTokens.Count; i++) if (sourceTokens[i].team == team) group.Add (sourceTokens[i]);
		team.map.MoveGroup (new GroupControl (team.map, group, target.transform.position));
	}

	protected abstract float getWeight ();
}
