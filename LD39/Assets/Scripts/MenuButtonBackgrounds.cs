﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (MeshRenderer), typeof (MeshFilter))]
public class MenuButtonBackgrounds : MonoBehaviour
{
	public Material baseMaterial;
	public Color colour;

	private MeshRenderer mr;
	private MeshFilter mf;

	Vector4[] pointsArray = new Vector4[10];
	Vector4[] pointColours = new Vector4[10];

	private void Awake ()
	{
		mr = GetComponent<MeshRenderer> ();
		mf = GetComponent<MeshFilter> ();
		if (mr.material != null) Destroy (mr.material);
		mr.material = new Material (baseMaterial);
		Mesh mesh = new Mesh ();
		mesh.vertices = new Vector3[]
		{
			new Vector3(-Camera.main.aspect, -1, 0),
			new Vector3(-Camera.main.aspect,  1, 0),
			new Vector3( Camera.main.aspect,  1, 0),
			new Vector3( Camera.main.aspect, -1, 0),
		};

		mesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3,
		};

		mesh.uv = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0),
		};
		mf.mesh = mesh;
	}

	private void Update ()
	{
		transform.localScale = Vector3.one * Camera.main.orthographicSize;
	}

	public void SetPoints (List<Vector2> points)
	{
		for (int i = 0; i < points.Count; i++)
		{
			pointsArray[i] = new Vector4 (points[i].x / Camera.main.orthographicSize, points[i].y / Camera.main.orthographicSize, 0.25f, 2);
			pointColours[i] = colour;
		}

		mr.material.SetInt ("numPoints", points.Count);
		mr.material.SetVectorArray ("points", pointsArray);
		mr.material.SetVectorArray ("pointColours", pointColours);
	}
}
