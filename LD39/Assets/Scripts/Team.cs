﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team
{
	public readonly Map map;
	public uint maxLevel = 3;
	public float tokenSize = 0.1f;
	public readonly Color colour = Color.white;

	public List<Building> buildings = new List<Building> ();
	public List<Token> tokens = new List<Token> ();
	private float baseTokenSize;

	public Team (Map map, Color colour, float baseTokenSize)
	{
		this.map = map;
		this.colour = colour;
		this.baseTokenSize = baseTokenSize;
	}

	internal void RemoveToken (Token token)
	{
		if (tokens.Contains (token)) tokens.Remove (token);
		map.RemoveToken (token);
	}

	public void SpawnToken (Vector2 position)
	{
		if (map.CanSpawnMoreTokens ())
		{
			Token token = new Token (this, position);
			tokens.Add (token);
			map.AddToken (token);
		}
		else
		{
			List<Token> sortedTokens = new List<Token> (tokens);
			sortedTokens.Sort ((a, b) => 
			{
				return (a.level * a.level * Vector2.SqrMagnitude (a.position - position)).CompareTo (b.level * b.level * Vector2.SqrMagnitude (b.position - position));
			});

			for (int i = 0; i < sortedTokens.Count; i++)
			{
				if (sortedTokens[i].level < maxLevel)
				{
					sortedTokens[i].level++;
					sortedTokens[i].currentHealth += sortedTokens[i].maxHealthPerLevel;
					break;
				}
			}

			Debug.Log ("Team.SpawnToken: Max tokens reached");
		}
	}

	public virtual void OnLoseBuilding (Building building) { }
	public virtual void OnGainBuilding (Building building) { }
	public virtual void Update (float deltaTime) { }
}
