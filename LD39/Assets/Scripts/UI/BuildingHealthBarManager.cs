﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingHealthBarManager : MonoBehaviour
{
	public Vector2 barOffset = Vector2.zero;
	public BuildingHealthIndicator healthBarPrefab;

	private Dictionary<Building, BuildingHealthIndicator> healthBars = new Dictionary<Building, BuildingHealthIndicator> ();
	private Queue<BuildingHealthIndicator> healthBarPool = new Queue<BuildingHealthIndicator> ();

	private void Update ()
	{
		foreach (KeyValuePair<Building, BuildingHealthIndicator> item in healthBars)
		{
			item.Value.value = item.Key.teamControl;
			if (item.Key.currentTeam != null) item.Value.SetColour (item.Key.currentTeam.colour);
			else if (item.Key.newTeam != null) item.Value.SetColour (item.Key.newTeam.colour);
			else item.Value.SetColour (item.Key.defaultColour);
		}
	}

	public void AddBuilding (Building building)
	{
		if (healthBars.ContainsKey (building)) Debug.LogWarning ("BuildingHealthBarManager.AddBuilding: Already got building");
		BuildingHealthIndicator newHealthBar = getHealthBar ();
		newHealthBar.gameObject.SetActive (true);
		newHealthBar.value = building.teamControl;
		if (building.currentTeam != null) newHealthBar.SetColour (building.currentTeam.colour);
		else if (building.newTeam != null) newHealthBar.SetColour (building.newTeam.colour);
		else newHealthBar.SetColour (building.defaultColour);

		Vector3 pos = (Vector2)building.transform.position + barOffset;
		pos.z = -5;
		newHealthBar.transform.position = pos;

		healthBars[building] = newHealthBar;
	}

	public void Clear ()
	{
		foreach (KeyValuePair<Building, BuildingHealthIndicator> item in healthBars)
		{
			item.Value.gameObject.SetActive (false);
			healthBarPool.Enqueue (item.Value);
		}

		healthBars.Clear ();
	}

	private BuildingHealthIndicator getHealthBar ()
	{
		if (healthBarPool.Count > 0) return healthBarPool.Dequeue ();
		else return Instantiate (healthBarPrefab, transform);
	}
}
