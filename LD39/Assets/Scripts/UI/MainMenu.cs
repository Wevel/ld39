﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
	public static bool Paused { get; private set; }
	public static bool IsPlayingGame { get; private set; }
	public static bool GameEnded { get; private set; }

	public Game gameManager;
	public MenuDisplay menuDisplay;

	private List<MapData> maps = new List<MapData> ();
	private MapData aiOnlyMap;

	public GameObject levelSelectButton;
	public GameObject mainMenuButton;
	public GameObject continueButton;
	public GameObject level0Button;
	public GameObject level1Button;
	public GameObject level2Button;
	public GameObject level3Button;
	public GameObject muteButton;
	public GameObject unmuteButton;
	public GameObject pauseButton;
	public GameObject winScreen;
	public GameObject loseScreen;

	private bool showLevelSelectMenu = false;
	private int currentLevel = -1;

	private void Awake ()
	{
		aiOnlyMap = new MapData (40, 5, (map) =>
		{
			map.AddTeam (new Vector2 (18, 18), new Color32 (21, 129, 0, 255));
			map.AddTeam (new Vector2 (22, 22), new Color32 (120, 0, 0, 255));
			map.AddTeam (new Vector2 (18, 22), new Color32 (200, 130, 0, 255));
			map.AddTeam (new Vector2 (22, 18), new Color32 (0, 65, 141, 255));

			map.AddBuilding (new Vector2 (16, 20), 1);
			map.AddBuilding (new Vector2 (24, 20), 1);
			map.AddBuilding (new Vector2 (20, 16), 1);
			map.AddBuilding (new Vector2 (20, 24), 1);

			map.AddBuilding (new Vector2 (20, 20), 3);
		});

		maps.Add (new MapData (40, 3, (map) =>
		{
			map.AddTeam (new Vector2 (12, 20), new Color32 (21, 129, 0, 255), true);
			map.AddTeam (new Vector2 (20, 20), new Color32 (120, 0, 0, 255));
			map.AddTeam (new Vector2 (22, 24), new Color32 (200, 130, 0, 255));
			map.AddTeam (new Vector2 (22, 16), new Color32 (0, 65, 141, 255));

			map.AddBuilding (new Vector2 (20.5f, 22.5f), 1);
			map.AddBuilding (new Vector2 (21.5f, 22), 1);

			map.AddBuilding (new Vector2 (20.5f, 17.5f), 1);
			map.AddBuilding (new Vector2 (21.5f, 18), 1);

			map.AddBuilding (new Vector2 (18, 20.5f), 1);
			map.AddBuilding (new Vector2 (18, 19.5f), 1);

			map.AddBuilding (new Vector2 (13, 21.5f), 1);
			map.AddBuilding (new Vector2 (13, 18.5f), 1);

			map.AddBuilding (new Vector2 (14, 20), 3);

			map.AddBuilding (new Vector2 (16, 20), 2);
		}));

		maps.Add (new MapData (40, 3, (map) =>
		{
			map.AddTeam (new Vector2 (17, 23), new Color32 (21, 129, 0, 255), true, 2);
			map.AddTeam (new Vector2 (23, 23), new Color32 (120, 0, 0, 255));
			map.AddTeam (new Vector2 (23, 17), new Color32 (200, 130, 0, 255));
			map.AddTeam (new Vector2 (17, 17), new Color32 (0, 65, 141, 255));

			map.AddBuilding (new Vector2 (18.5f, 23), 1);
			map.AddBuilding (new Vector2 (21.5f, 23), 1);
			map.AddBuilding (new Vector2 (18.5f, 17), 1);
			map.AddBuilding (new Vector2 (21.5f, 17), 1);

			map.AddBuilding (new Vector2 (23, 18.5f), 1);
			map.AddBuilding (new Vector2 (23, 21.5f), 1);
			map.AddBuilding (new Vector2 (17, 18.5f), 1);
			map.AddBuilding (new Vector2 (17, 21.5f), 1);

			map.AddBuilding (new Vector2 (20, 23), 2);
			map.AddBuilding (new Vector2 (20, 17), 2);
			map.AddBuilding (new Vector2 (23, 20), 2);
			map.AddBuilding (new Vector2 (17, 20), 2);

			map.AddBuilding (new Vector2 (19, 21), 3);
			map.AddBuilding (new Vector2 (21, 19), 3);

			map.AddBuilding (new Vector2 (15.5f, 23), 1);
			map.AddBuilding (new Vector2 (17, 24.5f), 1);
		}));

		maps.Add (new MapData (40, 3, (map) =>
		{
			map.AddTeam (new Vector2 (16, 24), new Color32 (21, 129, 0, 255), true, 3);
			map.AddTeam (new Vector2 (24, 24), new Color32 (120, 0, 0, 255));
			map.AddTeam (new Vector2 (24, 16), new Color32 (200, 130, 0, 255));
			map.AddTeam (new Vector2 (16, 16), new Color32 (0, 65, 141, 255));

			map.AddBuilding (new Vector2 (14.5f, 25.5f), 1);
			map.AddBuilding (new Vector2 (25.5f, 25.5f), 1);
			map.AddBuilding (new Vector2 (25.5f, 14.5f), 1);
			map.AddBuilding (new Vector2 (14.5f, 14.5f), 1);

			map.AddBuilding (new Vector2 (18, 19), 1);
			map.AddBuilding (new Vector2 (19, 18), 1);

			map.AddBuilding (new Vector2 (22, 21), 1);
			map.AddBuilding (new Vector2 (21, 22), 1);

			map.AddBuilding (new Vector2 (18, 21), 1);
			map.AddBuilding (new Vector2 (21, 18), 1);

			map.AddBuilding (new Vector2 (22, 19), 1);
			map.AddBuilding (new Vector2 (19, 22), 1);

			map.AddBuilding (new Vector2 (17.5f, 22.5f), 2);
			map.AddBuilding (new Vector2 (22.5f, 22.5f), 2);
			map.AddBuilding (new Vector2 (22.5f, 17.5f), 2);
			map.AddBuilding (new Vector2 (17.5f, 17.5f), 2);

			map.AddBuilding (new Vector2 (20, 20), 4);
		}));

		maps.Add (new MapData(40, 3, (map) =>
		{
			map.AddTeam (new Vector2 (14, 20), new Color32 (21, 129, 0, 255), true, 3);
			map.AddTeam (new Vector2 (20, 26), new Color32 (120, 0, 0, 255));
			map.AddTeam (new Vector2 (26, 20), new Color32 (200, 130, 0, 255));
			map.AddTeam (new Vector2 (20, 14), new Color32 (0, 65, 141, 255));

			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 1) * new Vector2 (0, 6)), 1);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 2) * new Vector2 (0, 6)), 3);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 3) * new Vector2 (0, 6)), 1);

			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 5) * new Vector2 (0, 6)), 1);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 6) * new Vector2 (0, 6)), 3);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 7) * new Vector2 (0, 6)), 1);

			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 9) * new Vector2 (0, 6)), 1);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 10) * new Vector2 (0, 6)), 3);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 11) * new Vector2 (0, 6)), 1);

			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 13) * new Vector2 (0, 6)), 1);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 14) * new Vector2 (0, 6)), 3);
			map.AddBuilding (new Vector2 (20, 20) + (Vector2)(Quaternion.Euler (0, 0, 22.5f * 15) * new Vector2 (0, 6)), 1);

			//map.AddCircularObstacle (20, 20, 4);
			//map.AddRectangleObstacle (20, 20, 6, 6, 0);
		}));
	}

	private void Start ()
	{
		ReturnToMainMenu ();
	}

	private void Update ()
	{
		if (IsPlayingGame)
		{
			if (Input.GetKeyDown (KeyCode.Escape)) Paused = !Paused;
		}

		levelSelectButton.SetActive (!IsPlayingGame && !showLevelSelectMenu);
		mainMenuButton.SetActive (IsPlayingGame && Paused);
		continueButton.SetActive (IsPlayingGame && Paused);
		level0Button.SetActive (!IsPlayingGame && showLevelSelectMenu);
		level1Button.SetActive (!IsPlayingGame && showLevelSelectMenu);
		level2Button.SetActive (!IsPlayingGame && showLevelSelectMenu);
		level3Button.SetActive (!IsPlayingGame && showLevelSelectMenu);
		muteButton.SetActive ((Paused || !IsPlayingGame) && !showLevelSelectMenu && !Sound.Muted);
		unmuteButton.SetActive ((Paused || !IsPlayingGame) && !showLevelSelectMenu && Sound.Muted);
		pauseButton.SetActive (!Paused && IsPlayingGame);

		if (gameManager.map.playerTeam != null && !GameEnded)
		{
			if (gameManager.map.playerTeam.buildings.Count == 0)
			{
				loseScreen.SetActive (true);
				GameEnded = true;
			}
			else if (gameManager.map.playerTeam.buildings.Count == gameManager.map.buildings.Count)
			{
				winScreen.SetActive (true);
				GameEnded = true;
			}
		}
	}

	public void StartGame (int index)
	{
		if (index >= 0 && index < maps.Count) gameManager.StartMap (maps[index]);
		else Debug.LogError ("MainMenu.StartGame: Invalid map index - " + index + ", there are " + maps.Count + " maps");
		IsPlayingGame = true;
		Paused = false;
		showLevelSelectMenu = false;
		GameEnded = false;
		currentLevel = index;
	}

	public void ReturnToMainMenu ()
	{
		gameManager.StartMap (aiOnlyMap);
		Camera.main.transform.position = new Vector3 ((aiOnlyMap.mapSize / 2f) + 4, aiOnlyMap.mapSize / 2f, -10);
		IsPlayingGame = false;
		Paused = false;
		showLevelSelectMenu = false;
		//showMainMenu ();
	}

	//private void showMainMenu ()
	//{
	//	MenuDisplay.ActionButtonData[] actions = new MenuDisplay.ActionButtonData[1];
	//	actions[0] = new MenuDisplay.ActionButtonData ("Level Select", () => { showLevelSelectMenu (); });
	//	//actions[1] = new MenuDisplay.ActionButtonData ("Main Menu", () => { ReturnToMainMenu (); });
	//	menuDisplay.Show (Camera.main.transform.position, 90, actions);
	//}

	//public void showLevelSelectMenu ()
	//{
	//	showLevelSelectMenu = true;

	//	//MenuDisplay.ActionButtonData[] actions = new MenuDisplay.ActionButtonData[maps.Count];
	//	//for (int i = 0; i < maps.Count; i++)
	//	//{
	//	//	int index = i;
	//	//	actions[i] = new MenuDisplay.ActionButtonData ((i + 1) + "", () => { StartGame (index); });
	//	//}
	//	//menuDisplay.Show (Camera.main.transform.position, 60, actions);//360 / maps.Count
	//}

	//private void showPauseMenu ()
	//{
	//	MenuDisplay.ActionButtonData[] actions = new MenuDisplay.ActionButtonData[2];
	//	actions[0] = new MenuDisplay.ActionButtonData ("Main Menu", () => { ReturnToMainMenu (); });
	//	actions[1] = new MenuDisplay.ActionButtonData ("Continue", () => { Paused = false; });
	//	menuDisplay.Show (Camera.main.transform.position, 90, actions);
	//}

	//private void hideMenu ()
	//{
	//	menuDisplay.Hide ();
	//}

	public void MuteAudio ()
	{
		Sound.Muted = true;
	}

	public void UnMuteAudio ()
	{
		Sound.Muted = false;
	}

	public void ShowLevelSelect ()
	{
		if (IsPlayingGame) ReturnToMainMenu ();
		showLevelSelectMenu = true;
	}

	public void ContinueGame ()
	{
		Paused = false;
	}

	public void PauseGame ()
	{
		Paused = true;
	}

	public void WonGame ()
	{
		winScreen.SetActive (false);
		if (currentLevel < maps.Count - 1)
		{
			StartGame (currentLevel + 1);
		}
		else
		{
			ReturnToMainMenu ();
		}
	}

	public void LoseGame ()
	{
		loseScreen.SetActive (false);
		ShowLevelSelect ();
	}
}
