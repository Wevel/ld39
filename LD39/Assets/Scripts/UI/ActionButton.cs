﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour
{
	public delegate void ButtonPressed ();

	//public Image image;
	public Text buttonText;
	
	private float size;
	private ButtonPressed onPressed;

	public void Setup (float size, Sprite sprite, string text, ButtonPressed onPressed)
	{
		buttonText.font.material.mainTexture.filterMode = FilterMode.Bilinear;
		this.size = size;
		this.onPressed = onPressed;

		//image.sprite = sprite;
		buttonText.text = text;

		//if (sprite == null) image.enabled = false;
		//else image.enabled = true;
	}

	public void OnButtonPressed ()
	{
		if (Vector2.Distance (Camera.main.ScreenToWorldPoint (Input.mousePosition), transform.position) < size)
		{
			if (onPressed != null) onPressed.Invoke ();
		}
	}

	private void OnDrawGizmosSelected ()
	{
		if (onPressed != null)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (transform.position, size);
		}
	}
}
