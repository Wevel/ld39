﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDisplay : Building
{
	public uint currentLevel;

	public void SetBuildType (uint level)// BuildingType type)
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		pos.z = -2;
		transform.position = pos;
		currentLevel = level;
		gameObject.SetActive (true);

		updatePoints ();
	}

	new private void Update ()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		pos.z = -2;
		transform.position = pos;
		if (map.CanBuild (transform.position, currentLevel)) UpdateColour (new Color32 (30, 100, 0, 255));
		else UpdateColour (new Color32 (180, 0, 0, 255));
	}
}
