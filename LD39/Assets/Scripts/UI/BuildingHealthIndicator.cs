﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingHealthIndicator : MonoBehaviour
{
	public Image bar;
	public Image background;

	private float _value;
	public float value
	{
		get
		{
			return _value;
		}
		set
		{
			_value = Mathf.Clamp01 (value);
			bar.transform.localScale = new Vector3 (_value, 1, 1);
		}
	}

	public void SetColour (Color colour)
	{
		bar.color = colour;
		//colour.a *= 0.5f;
		//.color = colour;
	}
}
