﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDisplay : MonoBehaviour
{
	public struct ActionButtonData
	{
		//public Sprite sprite;
		public string text;
		public ActionButton.ButtonPressed onPressed;

		public ActionButtonData (string text, ActionButton.ButtonPressed onPressed)
		{
			//this.sprite = sprite;
			this.text = text;
			this.onPressed = onPressed;
		}
	}

	public MenuButtonBackgrounds backgrounds;
	public Material baseMaterial;
	public float size = 5;
	public float partSize = 0.5f;

	public Transform actionButtonHolders;
	public ActionButton actionButtonPrefabs;

	private List<ActionButton> actionButtons = new List<ActionButton> (6);

	public void Show (Vector2 position, float partAngleSeperation, ActionButtonData[] actions)
	{
		StopAllCoroutines ();
		StartCoroutine (createParts (position, 60, actions));
	}

	public void Hide ()
	{
		StopAllCoroutines ();
		StartCoroutine (removeParts ());
	}

	private IEnumerator createParts (Vector2 position, float partAngleSeperation, ActionButtonData[] actions)
	{
		List<Vector2> positions;

		if (actionButtons.Count > 0)
		{
			for (int i = actionButtons.Count - 1; i >= 0; i--)
			{
				yield return new WaitForSeconds (0.1f);
				Destroy (actionButtons[i].gameObject);
				actionButtons.RemoveAt (i);

				positions = new List<Vector2> ();
				for (int a = 0; a < actionButtons.Count; a++) positions.Add (actionButtons[a].transform.localPosition);
				backgrounds.SetPoints (positions);
			}
		}

		yield return new WaitForSeconds (0.15f);

		actionButtonHolders.position = new Vector3 (position.x, position.y, transform.position.z);

		if (actions.Length > 6)
		{
			Debug.LogError ("ActionPicker.createParts: The maximum parts that can be displayed is 6");
		}

		float initialAngleOffset = ((actions.Length - 1) * partAngleSeperation / 2f);
		ActionButton button;

		for (int i = 0; i < Mathf.Min (6, actions.Length); i++)
		{
			yield return new WaitForSeconds (0.1f);
			button = Instantiate (
					actionButtonPrefabs,
					actionButtonHolders.position + (Quaternion.Euler (0, 0, initialAngleOffset - (i * partAngleSeperation)) * new Vector2 (0, partSize * Camera.main.orthographicSize)),
					Quaternion.Euler (0, 0, 0),// (initialAngleOffset - (i * partAngleSeperation)) / 2f),
					actionButtonHolders);
			button.transform.localScale = Vector3.one * Camera.main.orthographicSize * 10;
			button.Setup (partSize * Camera.main.orthographicSize, null, actions[i].text, actions[i].onPressed);//actions[i].sprite

			actionButtons.Add (button);

			positions = new List<Vector2> ();
			for (int a = 0; a < actionButtons.Count; a++) positions.Add (actionButtons[a].transform.localPosition);
			backgrounds.SetPoints (positions);
		}
	}

	private IEnumerator removeParts ()
	{
		List<Vector2> positions;

		for (int i = actionButtons.Count - 1; i >= 0; i--)
		{
			yield return new WaitForSeconds (0.1f);
			Destroy (actionButtons[i].gameObject);
			actionButtons.RemoveAt (i);

			positions = new List<Vector2> ();
			for (int a = 0; a < actionButtons.Count; a++) positions.Add (actionButtons[a].transform.localPosition);
			backgrounds.SetPoints (positions);
		}
	}
}
