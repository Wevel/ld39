﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public float zoomSpeed = 1;
	public float minZoom = 3;
	public float maxZoom = 20;

	private Vector3 lastPosition;

	void Update ()
	{
		if (MainMenu.IsPlayingGame && !MainMenu.Paused)
		{
			Camera mainCamera = Camera.main;

			if (mainCamera != null)
			{
				if (mainCamera.pixelRect.Contains (Input.mousePosition))
				{
					if (Input.GetMouseButtonDown (2)) lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

					Vector3 currentPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition); ;

					if (Input.GetMouseButton (2))
					{
						mainCamera.transform.Translate (lastPosition - currentPosition);
					}
					else
					{
						mainCamera.orthographicSize -= mainCamera.orthographicSize * Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;

						if (mainCamera.orthographicSize <= minZoom) mainCamera.orthographicSize = minZoom;
						else if (mainCamera.orthographicSize >= maxZoom) mainCamera.orthographicSize = maxZoom;
						else mainCamera.transform.Translate ((Vector2)((currentPosition - transform.position) * Input.GetAxis ("Mouse ScrollWheel")), Space.World);
					}

					lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);
				}
			}
		}
	}
}
