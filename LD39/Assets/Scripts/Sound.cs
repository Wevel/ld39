﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
	public static bool Muted = false;

	private static Sound instance;

	public static void PlayPopSound ()
	{
		instance.playPopSound ();
	}

	public AudioClip[] popFxClips;

	public AudioSource musicSource;
	public AudioSource fxSource;

	private void playPopSound ()
	{
		if (fxSource.isPlaying) return;
		fxSource.clip = popFxClips[Random.Range (0, popFxClips.Length)];
		fxSource.Play ();
	}

	private void Awake ()
	{
		instance = this;
	}

	private void Update ()
	{
		musicSource.mute = Muted;
		fxSource.mute = Muted;
	}

	private void OnDestroy ()
	{
		instance = null;
	}
}
