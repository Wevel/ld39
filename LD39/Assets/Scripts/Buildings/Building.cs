﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (MeshRenderer), typeof (MeshFilter))]
public class Building : MonoBehaviour
{
	public Color defaultColour = new Color32 (70, 70, 70, 255);//56

	public float damageRate = 0.1f;
	public float size = 0.25f;
	public float teamChangeTimeDelay = 1;
	public Material baseMaterial;
	public float generationTimeDelay = 1;
	public uint level = 1;

	public Map map;

	public Team currentTeam { get; private set; }
	public Team newTeam { get; private set; }
	public float teamControl { get; private set; }

	private MeshRenderer mr;
	private MeshFilter mf;

	public List<Vector2> partOffsets = new List<Vector2> ();
	private Vector4[] points = new Vector4[4];
	private Vector4[] pointColours = new Vector4[4];

	private float generationTimer = 0;

	private void Awake ()
	{
		mr = this.GetComponent<MeshRenderer> ();
		mf = this.GetComponent<MeshFilter> ();

		teamControl = 1;
	}

	private void Start ()
	{
		if (mr.material != null) Destroy (mr.material);
		mr.material = new Material (baseMaterial);
		updatePoints ();
		if (currentTeam != null) UpdateColour (currentTeam.colour * defaultColour);
	}

	protected virtual void Update ()
	{
		if (MainMenu.Paused) return;

		if (currentTeam != null)
		{
			if (generationTimer > generationTimeDelay)
			{
				while (generationTimer > generationTimeDelay)
				{
					generationTimer -= generationTimeDelay;
					currentTeam.SpawnToken ((Vector2)transform.position + new Vector2 (Random.Range (-size / 2f, size / 2f), Random.Range (-size / 2f, size / 2f)));
				}
			}
			else
			{
				generationTimer += Time.deltaTime * level;
			}
		}

		List<Token> nearbyTokens = map.GetTokensInRange (transform.position, size * 1.5f);
		for (int i = 0; i < nearbyTokens.Count; i++)
		{
			if (Vector2.Distance (nearbyTokens[i].position, transform.position) > size * 0.75f)
			{
				if ((currentTeam == null) ||
					(nearbyTokens[i].team == currentTeam && teamControl < 1) ||
					nearbyTokens[i].team != currentTeam)
				{
					nearbyTokens[i].velocity += ((Vector2)transform.position - nearbyTokens[i].position) * Time.deltaTime * 3;
				}
				else
				{
					nearbyTokens[i].velocity += ((Vector2)transform.position - nearbyTokens[i].position) * Time.deltaTime * 2;
				}
			}
			else
			{
				AttackWithToken (nearbyTokens[i]);
			}
		}
	}

	public void SetTeam (Team team)
	{
		if (team != currentTeam)
		{
			Team oldTeam = currentTeam;

			currentTeam = team;
			generationTimer = 0;

			if (currentTeam != null) UpdateColour (currentTeam.colour * defaultColour);
			else UpdateColour (defaultColour);
			teamControl = 1;
			newTeam = null;

			if (oldTeam != null)
			{
				if (oldTeam.buildings.Contains (this)) oldTeam.buildings.Remove (this);
				oldTeam.OnLoseBuilding (this);
			}
			if (currentTeam != null)
			{
				if (!currentTeam.buildings.Contains (this)) currentTeam.buildings.Add (this);
				currentTeam.OnGainBuilding (this);
			}
		}
	}

	public void UpdateColour (Color newColour)
	{
		//currentColour = newColour;
		for (int i = 0; i < partOffsets.Count; i++) pointColours[i] = newColour;
		mr.material.SetVectorArray ("pointColours", pointColours);
	}

	public bool AttackWithToken (Token token)
	{
		if (currentTeam != null)
		{
			if (token.team == currentTeam)
			{
				// Heal
				if (teamControl < 1)
				{
					teamControl += Time.deltaTime * damageRate / level / level;
					token.ApplyDamage (Time.deltaTime * 2.5f); // Applying half damage to token if healing
					return false;
				}
				else
				{
					teamControl = 1;
					return true;
				}
			}
			else
			{
				// Attack
				if (teamControl > 0)
				{
					teamControl -= Time.deltaTime * damageRate / level / level;
					token.ApplyDamage (Time.deltaTime * 5);
					return false;
				}
				else
				{
					teamControl = 0;
					currentTeam = null;
					UpdateColour (defaultColour);
					return true;
				}
			}
		}
		else
		{
			if (newTeam == null)
			{
				newTeam = token.team;
				teamControl = Time.deltaTime * damageRate / level / level;
				token.ApplyDamage (Time.deltaTime * 5);
				return false;
			}
			else
			{
				if (newTeam == token.team)
				{
					if (teamControl < 1)
					{
						teamControl += Time.deltaTime * damageRate / level / level;
						token.ApplyDamage (Time.deltaTime * 5);
						return false;
					}
					else
					{
						teamControl = 1;
						newTeam = null;
						SetTeam (token.team);
						return true;
					}
				}
				else
				{
					if (teamControl > 0)
					{
						teamControl -= Time.deltaTime * damageRate / level / level;
						token.ApplyDamage (Time.deltaTime * 5);
					}
					else
					{
						teamControl = 0;
						newTeam = null;
					}

					return false;
				}
			}
		}
	}

	protected void updatePoints ()
	{
		partOffsets = getDisplayPoints ();

		Mesh mesh = new Mesh ();
		mesh.vertices = new Vector3[]
		{
			new Vector3(-size, -size, 0) * 5f,
			new Vector3(-size,  size, 0) * 5f,
			new Vector3( size,  size, 0) * 5f,
			new Vector3( size, -size, 0) * 5f,
		};

		mesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3,
		};

		mesh.uv = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0),
		};

		mf.mesh = mesh;

		for (int i = 0; i < partOffsets.Count; i++)
		{
			points[i] = new Vector4 (partOffsets[i].x, partOffsets[i].y, size, 2);
			pointColours[i] = defaultColour;
		}

		mr.material.SetFloat ("_Distance", map.baseTokenSize);
		mr.material.SetInt ("numPoints", partOffsets.Count);
		mr.material.SetVectorArray ("points", points);
		mr.material.SetVectorArray ("pointColours", pointColours);
	}

	private List<Vector2> getDisplayPoints ()
	{
		List<Vector2> points = new List<Vector2> ();

		switch (level)
		{
			case 1:
				points.Add (new Vector2 (0, 0));
				break;
			case 2:
				points.Add (new Vector2 (-size / 4f, 0));
				points.Add (new Vector2 (size / 4f, 0));
				break;
			case 3:
				float halfWidth = 0.75f * size / Mathf.Tan (60 * Mathf.Deg2Rad);

				points.Add (new Vector3 (-halfWidth, -size / 4f, 0));
				points.Add (new Vector3 (0, size / 2f, 0));
				points.Add (new Vector3 (halfWidth, -size / 4f, 0));

				break;
			case 4:
				points.Add (new Vector2 (-size / 4f, 0));
				points.Add (new Vector2 (size / 4f, 0));
				points.Add (new Vector2 (0, -size / 4f));
				points.Add (new Vector2 (0, size / 4f));
				break;
			default:
				Debug.LogError ("Building.getDisplayPoints: Level muse be either 1, 2, 3 or 4");
				break;
		}

		return points;
	}

	private void OnDestroy ()
	{
		if (mf != null && mf.mesh != null) Destroy (mf.mesh);
		if (mr != null && mr.material != null) Destroy (mr.material);
	}

	private void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.magenta;
		for (int i = 0; i < partOffsets.Count; i++) Gizmos.DrawWireSphere ((Vector2)transform.position + partOffsets[i], size * 1.5f);
	}
}
