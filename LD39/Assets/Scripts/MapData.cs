﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData
{
	public delegate void MapLoad (Map map);

	public readonly int mapSize;
	public readonly float cameraSize;
	public readonly MapLoad onLoadMap;

	public MapData (int mapSize, float cameraSize, MapLoad onLoadMap)
	{
		this.mapSize = mapSize;
		this.cameraSize = cameraSize;
		this.onLoadMap = onLoadMap;
	}
}
