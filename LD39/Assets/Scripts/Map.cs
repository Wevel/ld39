﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Map : MonoBehaviour
{
	public const int maxPoints = 1023;

	public BuildingHealthBarManager buildingHealthBarManager;

	public Color groupShade = Color.white;
	public int size = 10;
	public int displayChunkSize = 10;
	public int initialTokenCount = 4;
	public float buildingSize = 0.25f;
	public float baseTokenSize = 0.1f;
	public Material baseBuildingMaterial;
	public Material baseTeamTokenMaterial;
	public Material baseObstacleMaterial;
	public Color obstacleColour = Color.white;
	public Transform renderersHolder;

	public HashSet<Token> markedGroup;
	private List<GroupControl> movingGroups = new List<GroupControl> ();

	public int totalTokens;

	private int chunkCount;

	internal bool CanSpawnMoreTokens ()
	{
		return true;

		//int total = 0;
		//for (int i = 0; i < buildings.Count; i++) total += buildings[i].partOffsets.Count;
		//for (int i = 0; i < teams.Count; i++) total += teams[i].tokens.Count;
		//return total < maxPoints * numArrays;
	}

	public void MoveGroup (GroupControl groupControl)
	{
		for (int a = 0; a < groupControl.group.Count; a++)
		{
			for (int b = 0; b < movingGroups.Count; b++)
			{
				movingGroups[b].StealToken (groupControl.group[a]);
			}
		}

		movingGroups.Add (groupControl);
	}

	public Team playerTeam { get; private set; }

	public List<Team> teams = new List<Team> ();
	public List<Building> buildings = new List<Building> ();
	public List<Obstacle> obstacles = new List<Obstacle> ();

	private Chunk[,] chunks;
	private Cell[,] cells;

	private Mesh rendererMesh;

	public Cell this[float x, float y]
	{
		get
		{
			return this[Mathf.Clamp ((int)x, 0, size - 1), Mathf.Clamp ((int)y, 0, size - 1)];
		}
	}

	public Cell this[int x, int y]
	{
		get
		{
			if (x >= 0 && x < size && y >= 0 && y < size) return cells[x, y];
			else return null;
		}
	}

	private void Awake ()
	{
		rendererMesh = new Mesh ();
		//rendererMesh.vertices = new Vector3[]
		//{
		//	new Vector3(0, 0, 0),
		//	new Vector3(0, size, 0),
		//	new Vector3(size, size, 0),
		//	new Vector3(size, 0, 0),
		//};

		rendererMesh.vertices = new Vector3[]
		{
			new Vector3(0, 0, 0),
			new Vector3(0,  displayChunkSize, 0),
			new Vector3( displayChunkSize,  displayChunkSize, 0),
			new Vector3( displayChunkSize, 0, 0),
		};

		rendererMesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3,
		};

		rendererMesh.uv = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0),
		};

		chunkCount = Mathf.CeilToInt ((float)size / displayChunkSize);
		chunks = new Chunk[chunkCount, chunkCount];

		for (int x = 0; x < chunkCount; x++)
		{
			for (int y = 0; y < chunkCount; y++)
			{
				chunks[x, y] = new Chunk (
					getRenderer (),
					new Vector2 (x * displayChunkSize, y * displayChunkSize), 
					new Rect ((x * displayChunkSize) - 1, (y * displayChunkSize) - 1, displayChunkSize + 2, displayChunkSize + 2));
			}
		}
	}

	public void StartMap (MapData mapData)
	{
		size = mapData.mapSize;
		playerTeam = null;

		buildingHealthBarManager.Clear ();
		markedGroup.Clear ();
		movingGroups.Clear ();

		cells = new Cell[size, size];

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				cells[x, y] = new Cell (this, x, y);
			}
		}

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				cells[x, y].CalculateNeighbours ();
			}
		}

		teams.Clear ();
		buildings.Clear ();
		obstacles.Clear ();

		foreach (Transform child in transform) Destroy (child.gameObject);

		mapData.onLoadMap (this);

		if (playerTeam != null) Camera.main.transform.position = new Vector3 (playerTeam.buildings[0].transform.position.x, playerTeam.buildings[0].transform.position.y, -10);
		else Camera.main.transform.position = new Vector3 (size / 2f, size / 2f, -10);

		Camera.main.orthographicSize = mapData.cameraSize;
	}

	private void Update ()
	{
		if (MainMenu.Paused) return;

		updateTokens ();

		for (int i = movingGroups.Count - 1; i >= 0; i--) if (movingGroups[i].Update (1)) movingGroups.RemoveAt (i);

		for (int a = 0; a < teams.Count; a++)
		{
			teams[a].Update (Time.deltaTime);
			for (int i = teams[a].tokens.Count - 1; i >= 0; i--) teams[a].tokens[i].Update (Time.deltaTime);
			for (int i = teams[a].buildings.Count - 1; i >= 0; i--)
			{
				if (teams[a].buildings[i].currentTeam != teams[a]) teams[a].buildings.RemoveAt (i);
			}
		}

		for (int x = 0; x < chunkCount; x++)
		{
			for (int y = 0; y < chunkCount; y++)
			{
				chunks[x, y].ClearTokens ();
			}
		}

		Token token;
		Color colour;

		for (int i = 0; i < teams.Count; i++)
		{
			for (int a = 0; a < teams[i].tokens.Count; a++)
			{
				token = teams[i].tokens[a];
				colour = token.team.colour;
				if (markedGroup.Contains (token)) colour *= groupShade;
				
				for (int x = 0; x < chunkCount; x++)
				{
					for (int y = 0; y < chunkCount; y++)
					{
						if (chunks[x, y].bounds.Contains (token.position)) chunks[x, y].AddToken (token, colour);
					}
				}
			}
		}

		for (int x = 0; x < chunkCount; x++)
		{
			for (int y = 0; y < chunkCount; y++)
			{
				chunks[x, y].Complete ();
			}
		}

		totalTokens = 0;
		for (int i = 0; i < teams.Count; i++)
		{
			for (int a = 0; a < teams[i].tokens.Count; a++)
			{
				totalTokens++;
			}
		}
	}

	private void OnDestroy ()
	{
		for (int x = 0; x < chunkCount; x++)
		{
			for (int y = 0; y < chunkCount; y++)
			{
				chunks[x, y].Dispose ();
			}
		}
	}

	public Team AddTeam (Vector2 initialPosition, Color colour, bool isPlayer = false, uint startingBuildingLevel = 2)
	{
		Team team;

		if (isPlayer)
		{
			if (playerTeam != null) Debug.LogWarning ("Map.AddTeam: Already a player team set");
			team = new Team (this, colour, baseTokenSize);
			playerTeam = team;
		}
		else
		{
			team = new AITeam (this, colour, baseTokenSize);
		}
		
		// Need to add some initial tokens
		float initialAngle = Random.Range (0, 90);
		float deltaAngle = 360f / initialTokenCount * Random.Range (0.8f, 1.2f);
		for (int i = 1; i < initialTokenCount; i++) team.SpawnToken (initialPosition + (Vector2)(Quaternion.Euler (0, 0, initialAngle + (i * deltaAngle)) * new Vector2 (baseTokenSize / 2.5f, 0)));
		team.SpawnToken (initialPosition);

		teams.Add (team);

		// Add the generator that the team starts with
		AddBuilding (initialPosition, startingBuildingLevel).SetTeam (team);

		return team;
	}

	public Building AddBuilding (Vector2 position, uint level)
	{
		GameObject go = new GameObject ("Building", typeof (MeshRenderer), typeof (MeshFilter));
		go.layer = LayerMask.NameToLayer ("Building");
		go.transform.SetParent (this.transform);
		go.transform.localPosition = new Vector3(position.x, position.y, -1f);


		Building building = go.AddComponent<Building> ();
		building.map = this;
		building.baseMaterial = baseBuildingMaterial;
		building.size = GetActualBuildingSize(level);
		building.level = level;
		buildings.Add (building);

		buildingHealthBarManager.AddBuilding (building);

		return building;
	}

	public Obstacle AddRectangleObstacle (int centreX, int centreY, int sizeX, int sizeY, float rotation)
	{
		GameObject go = new GameObject ("Obstacle", typeof (MeshRenderer), typeof (MeshFilter));
		go.transform.SetParent (transform);
		go.transform.position = new Vector3(centreX, centreY, -4);
		go.transform.rotation = Quaternion.Euler (0, 0, rotation);

		Obstacle obstacle = go.AddComponent<Obstacle> ();
		obstacle.baseMaterial = baseObstacleMaterial;
		obstacle.colour = obstacleColour;
		obstacle.Setup (centreX, centreY, sizeX, sizeY, rotation);

		obstacles.Add (obstacle);
		return obstacle;
	}

	public Obstacle AddCircularObstacle (int centreX, int centreY, int radius)
	{
		GameObject go = new GameObject ("Obstacle", typeof (MeshRenderer), typeof (MeshFilter));
		go.transform.SetParent (transform);
		go.transform.position = new Vector3 (centreX, centreY, -4);
		go.transform.rotation = Quaternion.Euler (0, 0, 0);

		Obstacle obstacle = go.AddComponent<Obstacle> ();
		obstacle.baseMaterial = baseObstacleMaterial;
		obstacle.colour = obstacleColour;
		obstacle.Setup (centreX, centreY, radius);

		obstacles.Add (obstacle);
		return obstacle;
	}

	public bool TryBuild (Vector2 position, uint level)
	{
		if (CanBuild (position, level))
		{
			AddBuilding (position, level);
			return true;
		}

		return false;
	}

	public bool CanBuild (Vector2 position, uint level)
	{
		for (int i = 0; i < buildings.Count; i++)
		{
			for (int b = 0; b < buildings[i].partOffsets.Count; b++)
			{
				if (Vector2.Distance ((Vector2)buildings[i].transform.position + buildings[i].partOffsets[b], position) < (GetActualBuildingSize(level) + buildings[i].size) * 1.5f) return false;
			}
		}

		return true;
	}

	public Team GetBuildingTeam (Building building)
	{
		List<Token> tokens = GetTokensUnderBuilding (building);

		if (tokens.Count == 0) return null;

		Dictionary<Team, uint> teams = new Dictionary<Team, uint> ();

		for (int i = 0; i < tokens.Count; i++)
		{
			if (tokens != null)
			{
				if (!teams.ContainsKey (tokens[i].team)) teams[tokens[i].team] = 1;
				else teams[tokens[i].team]++;
			}
		}

		Team highestTeam = null;
		uint highestCount = 0;

		foreach (KeyValuePair<Team, uint> item in teams)
		{
			if (highestTeam == null || item.Value > highestCount)
			{
				highestTeam = item.Key;
				highestCount = item.Value;
			}
		}

		return highestTeam;
	}

	public List<Token> GetTokensUnderBuilding (Building building)
	{
		List<Token> tokens = new List<Token> ();
		List<Token> newTokens;

		for (int i = 0; i < building.partOffsets.Count; i++)
		{
			newTokens = GetTokensInRange ((Vector2)building.transform.position + building.partOffsets[i], building.size);
			for (int a = 0; a < newTokens.Count; a++) if (!tokens.Contains (newTokens[a])) tokens.Add (newTokens[a]);
		}

		return tokens;
	}

	public List<Token> GetTokensInRange (Vector2 position, float range)
	{
		List<Token> tokens = new List<Token> ();
		Cell centreCell = this[position.x, position.y];

		centreCell.GetTokensInRange (position, range, tokens);

		Vector2 fracPosition = position - new Vector2 (Mathf.FloorToInt (position.x), Mathf.FloorToInt (position.y));
		bool shouldUp = true;
		bool shouldDown = true;
		bool shouldLeft = true;
		bool shouldRight = true;

		if (range < 0.5f)
		{
			if (fracPosition.x < 0.5f) shouldRight = false;
			else if (fracPosition.x > 0.5f) shouldLeft = false;

			if (fracPosition.y < 0.5f) shouldUp = false;
			else if (fracPosition.y > 0.5f) shouldDown = false;
		}

		if (centreCell.Up != null && shouldUp) centreCell.Up.GetTokensInRange (position, range, tokens);
		if (centreCell.UpLeft != null && shouldUp && shouldLeft) centreCell.UpLeft.GetTokensInRange (position, range, tokens);
		if (centreCell.UpRight != null && shouldUp && shouldRight) centreCell.UpRight.GetTokensInRange (position, range, tokens);
		if (centreCell.Left != null && shouldLeft) centreCell.Left.GetTokensInRange (position, range, tokens);
		if (centreCell.Right != null && shouldRight) centreCell.Right.GetTokensInRange (position, range, tokens);
		if (centreCell.Down != null && shouldDown) centreCell.Down.GetTokensInRange (position, range, tokens);
		if (centreCell.DownLeft != null && shouldDown && shouldLeft) centreCell.DownLeft.GetTokensInRange (position, range, tokens);
		if (centreCell.DownRight != null && shouldDown && shouldRight) centreCell.DownRight.GetTokensInRange (position, range, tokens);

		return tokens;
	}

	public float GetActualBuildingSize (uint level)
	{
		return buildingSize + ((level - 1) * buildingSize * 0.5f);
	}

	public void AddToken (Token token)
	{
		this[token.position.x, token.position.y].AddToken (token);
	}

	public void RemoveToken (Token token)
	{
		this[token.position.x, token.position.y].RemoveToken (token);
	}

	private void updateTokens ()
	{
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				if (cells[x, y].tokens.Count > 0) cells[x, y].CheckTokenCellPositions ();
			}
		}

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				if (cells[x, y].tokens.Count > 0 || cells[x, y].newTokens.Count > 0) cells[x, y].PushNewTokens ();
			}
		}

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				if (cells[x, y].tokens.Count > 0) cells[x, y].CheckCollisions ();
			}
		}
	}

	private MeshRenderer getRenderer ()
	{
		// Create new renderer
		GameObject go = new GameObject ("PointsRenderer");
		go.transform.SetParent (renderersHolder);
		go.transform.localPosition = Vector3.zero;
				
		MeshFilter mf = go.AddComponent<MeshFilter> ();
		mf.sharedMesh = rendererMesh;

		MeshRenderer mr = go.AddComponent<MeshRenderer> ();
		mr.material = new Material (baseTeamTokenMaterial);
		mr.material.SetFloat ("_Distance", baseTokenSize);

		return mr;
	}

	private void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.green;
		for (int a = 0; a < teams.Count; a++)
		{
			for (int i = 0; i < teams[a].tokens.Count; i++) Gizmos.DrawWireSphere (teams[a].tokens[i].position, teams[a].tokens[i].currentSize);
		}
	}
}
