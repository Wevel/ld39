﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
	public Material baseMaterial;
	public Vector2 centre;
	public Color colour;
	public float rotation;

	private MeshRenderer mr;
	private MeshFilter mf;

	public List<Vector2> points = new List<Vector2> ();
	public List<Vector2> rotatedPoints = new List<Vector2> ();

	private void Awake ()
	{
		mr = GetComponent<MeshRenderer> ();
		mf = GetComponent<MeshFilter> ();
	}

	public void Setup (int centreX, int centreY, int sizeX, int sizeY, float rotation)
	{
		this.centre = new Vector2(centreX, centreY);
		this.rotation = rotation;

		Vector2 halfSize = new Vector2 (sizeX, sizeY) / 2f;
		float leftEdge = centre.x - halfSize.x;
		float bottomEdge = centre.y - halfSize.y;

		Mesh mesh = new Mesh ();
		mesh.vertices = new Vector3[]
		{
			new Vector3(-halfSize.x - 2, -halfSize.y - 2, 0),
			new Vector3(-halfSize.x - 2, halfSize.y + 2, 0),
			new Vector3(halfSize.x + 2, halfSize.y + 2, 0),
			new Vector3(halfSize.x + 2, -halfSize.y - 2, 0),
		};

		mesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3,
		};

		mesh.uv = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0),
		};

		mf.mesh = mesh;

		for (float x = 0.5f; x < sizeX - 0.5f; x += 0.5f)
		{
			for (float y = 0.5f; y < sizeY - 0.5f; y += 0.5f)
			{
				points.Add (new Vector2 (leftEdge + x + 0.25f, bottomEdge + y + 0.25f));
			}
		}

		Vector4[] pointsArray = new Vector4 [points.Count];
		Vector4[] pointColours = new Vector4[points.Count];
		for (int i = 0; i < points.Count; i++)
		{
			pointsArray[i] = new Vector4 (points[i].x - centreX, points[i].y - centreY, 0.25f, 2);
			pointColours[i] = colour;

			rotatedPoints.Add ((Vector2)(Quaternion.Euler (0, 0, rotation) * (points[i] - centre)) + centre);
		}
		
		if (mr.material != null) Destroy (mr.material);
		mr.material = new Material (baseMaterial);
		mr.material.SetFloat ("_Distance", 0.1f);
		mr.material.SetInt ("numPoints", points.Count);
		mr.material.SetVectorArray ("points", pointsArray);
		mr.material.SetVectorArray ("pointColours", pointColours);
	}

	public void Setup (int centreX, int centreY, int radius)
	{
		this.centre = new Vector2 (centreX, centreY);
		this.rotation = 0;

		Mesh mesh = new Mesh ();
		mesh.vertices = new Vector3[]
		{
			new Vector3(-radius - 2, -radius - 2, 0),
			new Vector3(-radius - 2, radius + 2, 0),
			new Vector3(radius + 2, radius + 2, 0),
			new Vector3(radius + 2, -radius - 2, 0),
		};

		mesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3,
		};

		mesh.uv = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(1, 0),
		};

		mf.mesh = mesh;

		for (float x = -radius; x <= radius; x += 0.5f)
		{
			for (float y = -radius; y <= radius; y += 0.5f)
			{
				if ((x * x) + (y * y) < radius * radius) points.Add (new Vector2 (centreX + x, centreY + y));
			}
		}

		Vector4[] pointsArray = new Vector4[points.Count];
		Vector4[] pointColours = new Vector4[points.Count];
		for (int i = 0; i < points.Count; i++)
		{
			pointsArray[i] = new Vector4 (points[i].x - centreX, points[i].y - centreY, 0.25f, 2);
			pointColours[i] = colour;

			rotatedPoints.Add ((Vector2)(Quaternion.Euler (0, 0, rotation) * (points[i] - centre)) + centre);
		}

		if (mr.material != null) Destroy (mr.material);
		mr.material = new Material (baseMaterial);
		mr.material.SetFloat ("_Distance", 0.1f);
		mr.material.SetInt ("numPoints", points.Count);
		mr.material.SetVectorArray ("points", pointsArray);
		mr.material.SetVectorArray ("pointColours", pointColours);
	}

	private void OnDestroy ()
	{
		if (mf != null && mf.mesh != null) Destroy (mf.mesh);
		if (mr != null && mr.material != null) Destroy (mr.material);
	}
}
