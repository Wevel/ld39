﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
	public Map map;

	private HashSet<Token> currentGroup = new HashSet<Token> ();
	private float groupSearchRange = 0.5f;
	private bool isMakingGroup;

	private void Awake ()
	{
		map.markedGroup = currentGroup;
	}

	void Update ()
	{
		if (MainMenu.Paused || MainMenu.GameEnded) return;

		if (Input.GetMouseButtonUp (1))
		{
			isMakingGroup = false;
			currentGroup.Clear ();
		}
		else if (Input.GetMouseButtonDown (0))
		{
			isMakingGroup = true;
			currentGroup.Clear ();
		}
		else if (Input.GetMouseButtonUp (0))
		{
			if (isMakingGroup)
			{
				isMakingGroup = false;
				if (currentGroup.Count > 0)
				{
					GroupControl newGroup = new GroupControl (map, currentGroup, Camera.main.ScreenToWorldPoint (Input.mousePosition));
					currentGroup.Clear ();
					map.MoveGroup (newGroup);
				}
			}
		}
		else if (!Input.GetMouseButton (0))
		{
			isMakingGroup = false;
			currentGroup.Clear ();
		}

		if (isMakingGroup)
		{
			List<Token> newTokens = map.GetTokensInRange (Camera.main.ScreenToWorldPoint (Input.mousePosition), groupSearchRange);
			for (int i = 0; i < newTokens.Count; i++) if (newTokens[i].team == map.playerTeam && !currentGroup.Contains (newTokens[i])) currentGroup.Add (newTokens[i]);
		}
	}

	public void StartMap (MapData mapData)
	{
		currentGroup.Clear ();
		map.StartMap (mapData);
	}

	private void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere ((Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition), groupSearchRange);
	}
}
