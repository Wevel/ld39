﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupControl
{
	public readonly List<Token> group;
	public readonly Vector2 target;

	private Vector2 start;
	private float speed = 0;
	private float totalDistance;
	//private float reachedTargetDistance;
	private Building targetBuilding;

	public bool Update (float travelSpeed)
	{
		speed = Mathf.Lerp (speed, travelSpeed, Time.deltaTime * 5);

		Vector2 centre = Vector2.zero;
		for (int i = group.Count - 1; i >= 0; i--)
		{
			if (group[i].isAlive)
			{
				if (Vector2.SqrMagnitude (target - group[i].position) > 0.25f)
				{
					centre += group[i].position;
				}
				else
				{
					if (targetBuilding == null)
					{
						group.RemoveAt (i);
					}
					else
					{
						if (targetBuilding.AttackWithToken (group[i])) group.RemoveAt (i);
						else centre += group[i].position;
					}
				}
			}
			else
			{
				group.RemoveAt (i);
			}
		}

		centre /= group.Count;
		float centreDistance = Vector2.Distance (target, centre);
		Vector2 acceleration;

		for (int i = 0; i < group.Count; i++)
		{
			acceleration = (target - group[i].position).normalized * speed * 3;
			acceleration *= Mathf.Clamp (1 + ((centreDistance - Vector2.Distance (target, group[i].position)) / totalDistance), 0.9f, 1.1f);
			group[i].velocity = Vector2.ClampMagnitude (group[i].velocity + acceleration * Time.deltaTime, travelSpeed);
		}

		return group.Count == 0;
	}

	public void StealToken (Token token)
	{
		if (group.Contains (token)) group.Remove (token);
	}

	public GroupControl (Map map, HashSet<Token> currentGroup, Vector2 target)
	{
		target = new Vector2 (Mathf.Clamp (target.x, 2, map.size - 2), Mathf.Clamp (target.y, 2, map.size - 2));

		group = new List<Token> (currentGroup);
		this.target = target;

		start = Vector2.zero;
		for (int i = 0; i < group.Count; i++) start += group[i].position;
		start /= group.Count;

		totalDistance = Vector2.Distance (start, target);

		float closestDistance = float.MaxValue;
		float tmpDistance;

		for (int i = 0; i < map.buildings.Count; i++)
		{
			tmpDistance = Vector2.Distance (target, map.buildings[i].transform.position);
			if (tmpDistance < 0.25f)
			{
				if (targetBuilding == null || tmpDistance < closestDistance)
				{
					targetBuilding = map.buildings[i];
					closestDistance = tmpDistance;
				}
			}
		}

		if (targetBuilding != null) target = targetBuilding.transform.position;
		else if (totalDistance < 0.25f) group = new List<Token> ();
	}

	public GroupControl (Map map, List<Token> currentGroup, Vector2 target)
	{
		target = new Vector2 (Mathf.Clamp (target.x, 2, map.size - 2), Mathf.Clamp (target.y, 2, map.size - 2));

		group = new List<Token> (currentGroup);
		this.target = target;

		start = Vector2.zero;
		for (int i = 0; i < group.Count; i++) start += group[i].position;
		start /= group.Count;

		totalDistance = Vector2.Distance (start, target);

		float closestDistance = float.MaxValue;
		float tmpDistance;

		for (int i = 0; i < map.buildings.Count; i++)
		{
			tmpDistance = Vector2.Distance (target, map.buildings[i].transform.position);
			if (tmpDistance < 0.25f)
			{
				if (targetBuilding == null || tmpDistance < closestDistance)
				{
					targetBuilding = map.buildings[i];
					closestDistance = tmpDistance;
				}
			}
		}

		if (targetBuilding != null) target = targetBuilding.transform.position;
		else if (totalDistance < 0.25f) group = new List<Token> ();
	}
}