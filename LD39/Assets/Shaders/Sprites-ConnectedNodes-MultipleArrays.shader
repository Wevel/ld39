﻿Shader "Sprites/ConnectedNodes-MultipleArrays"
{
	Properties
	{
		_Distance ("Node Size", Float) = 0.1
		_ExpandRate ("Expand Rate", range(0.25, 2)) = 0.5
	}
	SubShader
	{
		Tags 
		{ 
			"Queue"="Transparent" 
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		
		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#define MAX_POINTS 1023
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 localVertex : TEXCOORD1;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.localVertex = v.vertex;
				o.uv = v.uv;
				return o;
			}

			uniform int numPoints_0;
			uniform float4 points_0[MAX_POINTS];
			uniform float4 pointColours_0[MAX_POINTS];
			//uniform int numPoints_1;
			//uniform float4 points_1[MAX_POINTS];
			//float4 pointColours_1[MAX_POINTS];
			//uniform int numPoints_2;
			//float4 points_2[MAX_POINTS];
			//float4 pointColours_2[MAX_POINTS];
			//uniform int numPoints_3;
			//float4 points_3[MAX_POINTS];
			//float4 pointColours_3[MAX_POINTS];

			float _Distance;
			float _ExpandRate;
			
			float smin(float a, float b, float k)
			{
				float res = exp (-k * a) + exp (-k * b);
				return -log (max (0.0001, res)) / k;
			}

			fixed4 frag (v2f IN) : SV_Target
			{
				fixed4 c = 0.7;
				float smoothedDist = 0.1;
				float minDist = 0.4;
				float modifiedDist;
				float tmpMinDist;

				for (int i = 0; i < numPoints_0; i++)
				{
					modifiedDist = distance (points_0[i].xy, IN.localVertex.xy) - (points_0[i].z * 0.5);
					smoothedDist = smin (smoothedDist, modifiedDist, _ExpandRate / _Distance);
					tmpMinDist = modifiedDist * max (1, points_0[i].w - 1);
					if (tmpMinDist <= minDist)
					{
						c = pointColours_0[i];
						minDist = tmpMinDist;
					}
				}

				/*for (int i = 0; i < numPoints_1; i++)
				{
					modifiedDist = distance (points_1[i].xy, IN.localVertex.xy) - (points_1[i].z * 0.5);
					smoothedDist = smin (smoothedDist, modifiedDist, _ExpandRate / _Distance);
					tmpMinDist = modifiedDist * max (1, points_1[i].w - 1);
					if (tmpMinDist <= minDist)
					{
						c = pointColours_1[i];
						minDist = tmpMinDist;
					}
				}

				for (int i = 0; i < numPoints_2; i++)
				{
					modifiedDist = distance (points_2[i].xy, IN.localVertex.xy) - (points_2[i].z * 0.5);
					smoothedDist = smin (smoothedDist, modifiedDist, _ExpandRate / _Distance);
					tmpMinDist = modifiedDist * max (1, points_2[i].w - 1);
					if (tmpMinDist <= minDist)
					{
						c = pointColours_2[i];
						minDist = tmpMinDist;
					}
				}

				for (int i = 0; i < numPoints_3; i++)
				{
					modifiedDist = distance (points_3[i].xy, IN.localVertex.xy) - (points_3[i].z * 0.5);
					smoothedDist = smin (smoothedDist, modifiedDist, _ExpandRate / _Distance);
					tmpMinDist = modifiedDist * max (1, points_3[i].w - 1);
					if (tmpMinDist <= minDist)
					{
						c = pointColours_3[i];
						minDist = tmpMinDist;
					}
				}*/

				c *= 1 - minDist;
				c.a *= 1 - minDist;
				c.a = lerp (c.a, 0, clamp (smoothedDist / _Distance, 0, 1));
				c.rgb *= c.a;

				return c;
			}
			ENDCG
		}
	}
}
